# gpu6
CUDA_VISIBLE_DEVICES=1 python train.py --approach=fotsgirwot --net=fotsgirwot --dataset_name=syn --dataset_root=/home/data/SynthText/ --log_root=~/log/fots/Rotation/  --syn --num_epochs=2 --decay_step 1 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --num_workers=4 --pos_weight=0.5 --lr=1e-3

#ic17
CUDA_VISIBLE_DEVICES=1 python train.py --approach=fotsgirwot --net=fotsgirwot --dataset_name=ic17 --dataset_root=~/data/fots/dataset17/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/fotsgirwot/syn/fotsgirwot_pweight0.50_ohem800-200_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model1_4999.pth --log_root=~/log/fots/Rotation/ --num_epochs=200 --decay_step 80 160 --ohem --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --pos_weight=0.5 --lr=1e-3


# ic15
CUDA_VISIBLE_DEVICES=1 python train.py --approach=fotsgirwot --net=fotsgirwot --dataset_name=ic15_13 --dataset_root=~/data/fots/dataset15_13/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/fotsgirwot/ic17/fotsgirwot_pweight0.50_ohem1102-275_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_119.pth --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=600 --decay_step 250 450 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140 --num_workers=4 --pos_weight=0.5 --lr=1e-3
