
# eval one
CUDA_VISIBLE_DEVICES=1 python validation_one_model.py --model_path=/home/chenyu/log/fots/Rotation/orngatesog/ic15_13/orngatesog_pweight0.50_ohem456-114_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_324.pth --score_map_thresh=0.8 --box_thresh=0.1 --nms_thres=0.2 --eval_test --dataset_root=/home/chenyu/data/fots/dataset15_13/ --dataset_name=ic15_13 --scale 1600 --lexicon_root=./lexicon_13_15/ --lexicon_protocol=weak
# multi size
CUDA_VISIBLE_DEVICES=0 python validation_one_model.py --model_path=/home/chenyu/log/fots/Rotation/fots_bc/ic15_13/fots_bc_pweight0.50_ohem456-114_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_399.pth --arange 0.5 0.93 0.02 --box_thresh=0.1 --nms_thres=0.15 --eval_test --dataset_root=/home/chenyu/data/fots/dataset15_13/ --dataset_name=ic15_13 --scale 2240 1920 1792 --lexicon_root=./lexicon_13_15/ --lexicon_protocol=general

# here
CUDA_VISIBLE_DEVICES=0 python validation_one_model.py --model_path=/home/cheny/log/fots/Rotation/fots_bc/ic171513_13/fots_bc_pweight0.50_ohem862-210_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_129.pth --arange 0.6 0.93 0.02 --box_thresh=0.1 --nms_thres=0.5 --eval_test --dataset_root=/home/cheny/data/fots/dataset171513_13/ --dataset_name=ic171513_13 --scale 920 640 800  --lexicon_root=./lexicon_13/ --lexicon_protocol=weak --noanalyse
CUDA_VISIBLE_DEVICES=2 python validation_one_model.py --model_path=/home/cheny/log/fots/Rotation/fotsgir/ic171513_13/fotsgir_pweight0.50_ohem862-210_ltheta10.00_lreg1.00_lrecog1.00_lr0.0005_m0.9/model_109.pth --arange 0.6 0.93 0.02 --box_thresh=0.1 --nms_thres=0.5 --eval_test --dataset_root=/home/cheny/data/fots/dataset171513_13/ --dataset_name=ic171513_13 --scale 920 640 800  --lexicon_root=./lexicon_13/ --lexicon_protocol=weak --noanalyse


# eval multiple

CUDA_VISIBLE_DEVICES=1 python validation_models_for_draw.py --model_root=/home/chenyu/log/fots/Rotation/fotsgir/ic15_13/fotsgir_pweight0.50_ohem456-114_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/ --score_map_thresh=0.8 --box_thresh=0.1 --nms_thres=0.2 --eval_test --dataset_root=/home/chenyu/data/fots/dataset15_13/ --dataset_name=ic15_13 --resize_to_val=2240 --lexicon_protocol=weak --lexicon_root=./lexicon_13_15/
