


CUDA_VISIBLE_DEVICES=3 python train.py --approach=east --optimizer=adam --balance1 --net=fots --log_root=~/log/fots/ --dataset_root=./dataset/ --trainfolder=trainset --eval_train --valfolder=valset --testfolder=testset --num_epochs=800 --decay_step 250 500 --lexicon_protocol=weak --save_inter_cp --save_cp_interval=50 --log_interval_iter=80 --debug_save_interval_iter=4400 --trim --num_workers=0 --compute_statistics --lambda_recog=0.0 --lambda_reg=1.0 --lr=5e-4
CUDA_VISIBLE_DEVICES=4 python train.py --approach=east --optimizer=adam --balance1 --net=fots --log_root=~/log/fots/ --dataset_root=./dataset/ --trainfolder=trainset --eval_train --valfolder=valset --testfolder=testset --num_epochs=800 --decay_step 250 500 --lexicon_protocol=weak --save_inter_cp --save_cp_interval=50 --log_interval_iter=80 --debug_save_interval_iter=4400 --trim --num_workers=0 --compute_statistics --lambda_recog=0.0 --lambda_reg=1.0 --lr=1e-3
CUDA_VISIBLE_DEVICES=5 python train.py --approach=east --optimizer=adam --balance1 --net=fots --log_root=~/log/fots/ --dataset_root=./dataset/ --trainfolder=trainset --eval_train --valfolder=valset --testfolder=testset --num_epochs=800 --decay_step 250 500 --lexicon_protocol=weak --save_inter_cp --save_cp_interval=50 --log_interval_iter=80 --debug_save_interval_iter=4400 --trim --num_workers=0 --compute_statistics --lambda_recog=0.0 --lambda_reg=1.0 --lr=2e-3


