CUDA_VISIBLE_DEVICES=1 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --lr=1e-3
CUDA_VISIBLE_DEVICES=2 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --lr=2e-3
CUDA_VISIBLE_DEVICES=3 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --lr=5e-3


#-- 12.26
#CUDA_VISIBLE_DEVICES=2 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --lambda_recog=2.0 --lr=5e-3
#CUDA_VISIBLE_DEVICES=4 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --lambda_recog=10.0 --lr=5e-3

# --12.27
CUDA_VISIBLE_DEVICES=2 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --num_workers=1 --lambda_recog=2.0 --lr=5e-3
CUDA_VISIBLE_DEVICES=3 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --num_workers=1 --lambda_recog=1.0 --lr=1e-2
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --num_workers=1 --lambda_recog=2.0 --lr=1e-2  暂时停不了
CUDA_VISIBLE_DEVICES=3 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --num_workers=1 --lambda_recog=1.0 --lr=2e-2
CUDA_VISIBLE_DEVICES=4 python train.py --approach=syn --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --trim --print_exception --num_workers=1 --lambda_recog=1.0 --pos_weight=0.45 --lr=1e-2



# 12-28


# 12-29
CUDA_VISIBLE_DEVICES=4 python train.py --approach=syn1 --net=fots --use_pretrain --resume_path=~/log/fots/syn/ --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.4 --lr=5e-3
CUDA_VISIBLE_DEVICES=4 python train.py --approach=syn1 --net=fots --use_pretrain --resume_path=~/log/fots/syn/ --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.5 --lr=5e-3

#TODO begin
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn1 --net=fots --use_pretrain --resume_path=~/log/fots/syn/ --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.3 --lr=5e-3
#TODO end

CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn1 --net=fots --use_pretrain --resume_path=~/log/fots/syn/ --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.5 --lr=2.5e-3


# 1-1
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn1 --net=fots --use_pretrain --resume_path=~/log/fots/syn/ --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.5 --lr=5e-3
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn1 --net=fots --use_pretrain --resume_path=~/log/fots/syn/ --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.5 --lr=5e-3
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn1 --net=fots --use_pretrain --resume_path=~/log/fots/syn/ --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.5 --lr=5e-3

# 1.10
@ compute positive
CUDA_VISIBLE_DEVICES=0 python train.py --approach=test --net=fots --compute_positive --log_root=~/log/fots/ --dataset_root=/home/data/SynthText/ --syn --num_epochs=3 --decay_step 2 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=1000 --debug_save_interval_iter=5000 --trim --num_workers=1 --lambda_recog=1.0 --pos_weight=0.5 --lr=5e-3


@ trian syn new
@ test
CUDA_VISIBLE_DEVICES=0 python train.py --approach=test --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --ohem_num_pi=1300 --ohem_reg_num_pi=325 --save_inter_cp_iter --save_cp_interval_iter=200 --log_interval_iter=5 --debug_save_interval_iter=200 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=5e-3


CUDA_VISIBLE_DEVICES=3 python train.py --approach=syn_new --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --ohem_num_pi=1300 --ohem_reg_num_pi=325 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=5e-3
CUDA_VISIBLE_DEVICES=3 python train.py --approach=syn_new --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=5e-3

CUDA_VISIBLE_DEVICES=4 python train.py --approach=syn_new --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --ohem_num_pi=1300 --ohem_reg_num_pi=325 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=2e-3
CUDA_VISIBLE_DEVICES=4 python train.py --approach=syn_new --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=2e-3

CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn_new --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --ohem_num_pi=1300 --ohem_reg_num_pi=325 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=1e-3
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn_new --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=3 --decay_step 1 2 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=1e-3



@ syn2, change net structure, add drop out, change dataloader

CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn2 --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=4 --decay_step 2 2 --ohem --ohem_num_pi=1200 --ohem_reg_num_pi=300 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=2e-3
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn2 --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=4 --decay_step 2 2 --ohem --ohem_num_pi=1200 --ohem_reg_num_pi=300 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=1e-3

CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn2 --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=4 --decay_step 2 2 --ohem --ohem_num_pi=1200 --ohem_reg_num_pi=300 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=5e-4
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn2 --net=fots --log_root=~/log/fots/ --dataset_root=~/data/syn800k/SynthText/ --syn --num_epochs=4 --decay_step 2 2 --ohem --ohem_num_pi=1200 --ohem_reg_num_pi=300 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=0 --compute_statistics --pos_weight=0.5 --lr=2e-4





@ gpu5:
CUDA_VISIBLE_DEVICES=0 python train.py --approach=syn2 --log_str=continue_1e-3 --use_pretrain --resume_path=~/log/fots/syn2/fots_pweight0.50_ohem1200-300_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model2_54999.pth --resize_to=1280 --net=fots --log_root=~/log/fots/ --dataset_root=/home/data/SynthText/ --syn --num_epochs=4 --decay_step 4 --ohem --ohem_num_pi=1600 --ohem_reg_num_pi=400 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=2 --compute_statistics --pos_weight=0.5 --lr=1e-4

# ---1.18
@ lamda8:
CUDA_VISIBLE_DEVICES=5 python train.py --approach=syn2 --log_str=continue_1e-4 --use_pretrain --resume_path=~/log/fots/syn2/continue_1e-3_fots_pweight0.50_ohem1600-400_ltheta10.00_lreg1.00_lrecog1.00_lr0.0001_m0.9/model1_14999.pth --resize_to=1280 --net=fots --log_root=~/log/fots/ --dataset_root=/home/chenyu/data/SynthText/ --syn --num_epochs=4 --decay_step 4 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --trim --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-4