CUDA_VISIBLE_DEVICES=0 python validation_for_thresh.py --model_path=/home/chenyu/log/fots/icdar15/icdar2/fots_pweight0.50_ohem512-128_ltheta10.00_lreg1.00_lrecog2.00_lr0.0005_m0.9/model.pth --eval_train --eval_val --eval_test --score_map_thresh=0.7 --box_thresh=0.501 --nms_thres=0.20 --box_before_nms_num=20000 --box_after_nms_num=5000 --box_area_thresh=1 --debug_save_eval


# --1.10:
CUDA_VISIBLE_DEVICES=0 python validation_for_thresh.py --model_path=/home/chenyu/log/fots/icdar15/valid_base_fots_pweight0.50_ohem2048-512_ltheta10.00_lreg1.00_lrecog1.00_lr0.005_m0.9/model_49.pth --score_map_thresh=0.5 --box_thresh=0.5 --nms_thres=0.1 --box_before_nms_num=8000 --box_after_nms_num=500 --debug_save_eval --eval_train --eval_test --log_dir=/home/chenyu/log/fots/icdar15/temp/ --dataset_root=./dataset2/



# --1.15
CUDA_VISIBLE_DEVICES=2 python validation_one_model.py --model_path=/home/chenyu/log/fots/icdar17/fots_pweight0.50_ohem1280-320_ltheta10.00_lreg1.00_lrecog1.00_lr0.0005_m0.9/model_54.pth --eval_train --eval_val --eval_test --score_map_thresh=0.5 --box_thresh=0.1 --nms_thres=0.1 --box_before_nms_num=8000 --box_after_nms_num=500 --box_area_thresh=30 --debug_save_eval --log_dir=/home/chenyu/log/fots/icdar15_1/temp/ --net=fots_old


# --1.18
CUDA_VISIBLE_DEVICES=2 python validation_one_model.py --model_path=/home/cheny/log/fots/icdar17/fots_pweight0.50_ohem1280-320_ltheta10.00_lreg1.00_lrecog1.00_lr0.0005_m0.9/model_54.pth --dataset_root=/home/cheny/data/fots/dataset2/ --eval_train --eval_test --score_map_thresh=0.5 --box_thresh=0.1 --nms_thres=0.1 --box_before_nms_num=8000 --box_after_nms_num=500 --box_area_thresh=30 --debug_save_eval --log_dir=/home/cheny/log/fots/icdar17/temp/

# --1.20

@ eval for draw figure
CUDA_VISIBLE_DEVICES=0 python validation_models_for_draw.py --model_root=/home/cheny/log/fots/icdar15_1.19/fots_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr0.002_m0.9/ --score_map_thresh=0.5 --box_thresh=0.1 --nms_thres=0.1 --eval_test --log_dir=/home/cheny/log/fots/icdar15_1.19/temp/ --resize_to_val=2240
@ eval one
CUDA_VISIBLE_DEVICES=3 python validation_one_model.py --model_path=/home/cheny/log/fots/icdar15_1.19/fots_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_424.pth --eval_test --score_map_thresh=0.5 --box_thresh=0.1 --nms_thres=0.1 --box_before_nms_num=8000 --box_after_nms_num=500 --box_area_thresh=30 --debug_save_eval --log_dir=/home/cheny/log/fots/icdar15_1.19/temp_one/


# --2.13


@ for bc:
CUDA_VISIBLE_DEVICES=0 python validation_models_for_draw.py --model_root=/home/cheny/log/fots/Rotation/fots_orn_icdar15/orn_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr0.0001_m0.9/ --score_map_thresh=0.501 --box_thresh=0.1 --nms_thres=0.1 --debug_save_eval --eval_train --eval_test --log_dir=/home/cheny/log/fots/Rotation/fots_orn_icdar15/temp_1e-4/ --dataset_root=/home/cheny/data/fots/dataset15_13/ --resize_to_val=1920 --lexicon_protocol=weak



@ for orn:
CUDA_VISIBLE_DEVICES=0 python validation_models_for_draw.py --model_root=/home/cheny/log/fots/Rotation/fots_orn_icdar15/orn_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr1e-05_m0.9/ --score_map_thresh=0.7 --box_thresh=0.1 --nms_thres=0.2 --eval_test  --dataset_root=/home/cheny/data/fots/dataset15_13/ --resize_to_val=2240 --lexicon_protocol=weak
CUDA_VISIBLE_DEVICES=1 python validation_models_for_draw.py --model_root=/home/cheny/log/fots/Rotation/fots_orn_icdar15/orn_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr2e-05_m0.9/ --score_map_thresh=0.7 --box_thresh=0.1 --nms_thres=0.2 --eval_test  --dataset_root=/home/cheny/data/fots/dataset15_13/ --resize_to_val=2240 --lexicon_protocol=weak
CUDA_VISIBLE_DEVICES=2 python validation_models_for_draw.py --model_root=/home/cheny/log/fots/Rotation/fots_orn_icdar15/orn_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr5e-05_m0.9/ --score_map_thresh=0.7 --box_thresh=0.1 --nms_thres=0.2 --eval_test  --dataset_root=/home/cheny/data/fots/dataset15_13/ --resize_to_val=2240 --lexicon_protocol=weak


@ one model for bc
CUDA_VISIBLE_DEVICES=3 python validation_one_model.py --model_path=/home/cheny/log/fots/Rotation/fots_bc_icdar15/fots_bc_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_499.pth --arange 0.6 0.9 0.02 --box_thresh=0.1 --nms_thres=0.1 --debug_save_eval --eval_test --dataset_root=/home/cheny/data/fots/dataset15_13/ --resize_to_val=2240 --lexicon_protocol=weak
@ one model for orn
CUDA_VISIBLE_DEVICES=3 python validation_one_model.py --model_path=/home/cheny/log/fots/Rotation/fots_orn_icdar15/orn_pweight0.50_ohem672-168_ltheta10.00_lreg1.00_lrecog1.00_lr0.0001_m0.9/model_374.pth --arange 0.7 0.9 0.02  --box_thresh=0.1 --nms_thres=0.1 --debug_save_eval --eval_test --dataset_root=/home/cheny/data/fots/dataset15_13/ --net=orn --resize_to_val=2240 --lexicon_protocol=weak