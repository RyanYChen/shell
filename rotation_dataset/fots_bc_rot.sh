# ic17 gpu7

CUDA_VISIBLE_DEVICES=2 python train.py --approach=fots_bc_rot --net=fots_bc --dataset_name=ic17_rot --dataset_root=~/data/fots/dataset17_rot/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/fots_bc/syn/fots_bc_pweight0.50_ohem800-200_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model1_9999.pth --log_root=~/log/fots/Rotation/ --num_epochs=200 --decay_step 80 160 --ohem --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --pos_weight=0.5 --lr=1e-3


# ic15
CUDA_VISIBLE_DEVICES=2 python train.py --approach=fots_bc_rot --net=fots_bc --dataset_name=ic15_13_rot --dataset_root=~/data/fots/dataset15_13_rot/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/fots_bc_rot/ic17_rot/fots_bc_pweight0.50_ohem1020-250_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_59.pth --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=600 --decay_step 250 450 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140 --num_workers=4 --pos_weight=0.5 --resize_to_val=1800 --lr=1e-3

CUDA_VISIBLE_DEVICES=0 python train.py --approach=fots_bc_rot --net=fots_bc --log_str=continue --dataset_name=ic15_13_rot --dataset_root=~/data/fots/dataset15_13_rot/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/fots_bc_rot/ic15_13_rot/fots_bc_pweight0.50_ohem450-225_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_224.pth --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=376 --decay_step 26 226 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140 --num_workers=4 --pos_weight=0.5 --resize_to_val=1800 --lr=1e-3
CUDA_VISIBLE_DEVICES=1 python train.py --approach=fots_bc_rot --net=fots_bc --dataset_name=ic15_13_rot --dataset_root=~/data/fots/dataset15_13_rot/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/fots_bc_rot/ic17_rot/fots_bc_pweight0.50_ohem1020-250_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_59.pth --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=600 --decay_step 250 450 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140 --num_workers=4 --pos_weight=0.5 --resize_to_val=1800 --lr=5e-3


# ic17sub_rot   gpu4
CUDA_VISIBLE_DEVICES=6 python train.py --approach=fots_bc_rot --net=fots_bc --dataset_name=ic17sub_rot --dataset_root=~/data/fots/dataset17sub_rot/ --use_pretrain --resume_path=/home/chenyu/log/fots/Rotation/fots_bc/syn/fots_bc_pweight0.50_ohem800-200_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model1_9999.pth --eval_test --trim --log_root=~/log/fots/Rotation/ --num_epochs=600 --decay_step 250 450 --ohem --save_inter_cp --save_cp_interval=20 --eval_interval=20 --log_interval_iter=150 --num_workers=6 --pos_weight=0.5 --lr=1e-3
