@gpu6, 2.23
CUDA_VISIBLE_DEVICES=0 python train.py --approach=syn --net=orngatesog --base_channel=16 --log_root=~/log/fots/Rotation/orngatesog/ --dataset_root=/home/data/SynthText/ --syn --num_epochs=4 --decay_step 2 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3

@ gpu6 ic17
CUDA_VISIBLE_DEVICES=0 python train.py --approach=orngatesog --net=orngatesog --dataset_name=ic17 --base_channel=16 --dataset_root=~/data/fots/dataset17/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/orngatesog/syn/orngatesog_pweight0.50_ohem800-200_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model1_9999.pth --log_root=~/log/fots/Rotation/ --num_epochs=200 --decay_step 80 160 --ohem --ohem_num_pi=1280 --ohem_reg_num_pi=320 --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3

# ic15 gpu6 todo doing
CUDA_VISIBLE_DEVICES=3 python train.py --approach=orngatesog --net=orngatesog --dataset_name=ic15_13 --dataset_root=~/data/fots/dataset15_13/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/orngatesog/ic17/orngatesog_pweight0.50_ohem1280-320_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_199.pth --base_channel=16 --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=550 --decay_step 250 400 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140 --num_workers=4 --pos_weight=0.5 --lr=1e-3

