@gpu4
CUDA_VISIBLE_DEVICES=0 python train.py --approach=syn --net=orngate --base_channel=16 --log_root=~/log/fots/Rotation/orngate/ --dataset_root=/home/data/SynthText/ --syn --num_epochs=4 --decay_step 2 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3

# icdar17
@ gpu4
CUDA_VISIBLE_DEVICES=0 python train.py --approach=icdar17 --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/orngate/syn/orngate_pweight0.50_ohem800-200_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model0_69999.pth --net=orngate --base_channel=16 --log_root=~/log/fots/Rotation/orngate/ --dataset_root=~/data/fots/dataset17/ --num_epochs=300 --decay_step 100 200 --ohem --ohem_num_pi=1280 --ohem_reg_num_pi=320 --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3

@ continue
CUDA_VISIBLE_DEVICES=0 python train.py --approach=orngate --net=orngate --dataset_name=ic17 --dataset_root=~/data/fots/dataset17/ --log_str=continue --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/orngate/ic17/orngate_pweight0.50_ohem1280-320_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_59.pth --base_channel=16 --log_root=~/log/fots/Rotation/ --num_epochs=140 --decay_step 10 70 --ohem --ohem_num_pi=1280 --ohem_reg_num_pi=320 --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3



# icdar15 正在跑这个 @ gpu4
CUDA_VISIBLE_DEVICES=0 python train.py --approach=orngate --net=orngate --dataset_name=ic15_13 --dataset_root=~/data/fots/dataset15_13/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/orngate/ic17/continue_orngate_pweight0.50_ohem1280-320_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_89.pth --base_channel=16 --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=550 --decay_step 250 400 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140  --num_workers=4 --pos_weight=0.5 --lr=1e-3
CUDA_VISIBLE_DEVICES=1 python train.py --approach=orngate --net=orngate --dataset_name=ic15_13 --dataset_root=~/data/fots/dataset15_13/ --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/orngate/ic17/continue_orngate_pweight0.50_ohem1280-320_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_89.pth --base_channel=16 --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=550 --decay_step 250 400 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140  --num_workers=4 --pos_weight=0.5 --lr=5e-4




