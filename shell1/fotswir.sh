@ lamda8gpu

CUDA_VISIBLE_DEVICES=7 python train.py --approach=fotswir --net=fotswir --shared_channel=32 --dataset_name=syn --dataset_root=~/data/SynthText/ --log_root=~/log/fots/Rotation/  --syn --num_epochs=2 --decay_step 1 --ohem --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --num_workers=4 --pos_weight=0.5 --lr=1e-3

# ic17
CUDA_VISIBLE_DEVICES=7 python train.py --approach=fotswir --net=fotswir --shared_channel=32 --dataset_name=ic17 --dataset_root=~/data/fots/dataset17/ --use_pretrain --resume_path=/home/chenyu/log/fots/Rotation/fotswir/syn/fotswir_pweight0.50_ohem800-200_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model1_19999.pth --log_root=~/log/fots/Rotation/ --num_epochs=200 --decay_step 80 160 --ohem --ohem_num_pi=1280 --ohem_reg_num_pi=320 --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3
# continue ic17
CUDA_VISIBLE_DEVICES=0 python train.py --approach=fotswir --net=fotswir --shared_channel=32 --dataset_name=ic17 --dataset_root=~/data/fots/dataset17/ --use_pretrain --log_str=continue --resume_path=/home/cheny/log/fots/Rotation/fotswir/ic17/fotswir_pweight0.50_ohem1280-320_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model_49.pth --log_root=~/log/fots/Rotation/ --num_epochs=150 --decay_step 30 110 --ohem --ohem_num_pi=1280 --ohem_reg_num_pi=320 --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3
