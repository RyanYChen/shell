@gpu6, 2.23
CUDA_VISIBLE_DEVICES=1 python train.py --approach=syn --net=fotsgatesog --resize_to=1280 --log_root=~/log/fots/Rotation/fotsgatesog/ --dataset_root=/home/data/SynthText/ --syn --num_epochs=2 --decay_step 1 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3

@ gpu6, 2.24, continue syn
CUDA_VISIBLE_DEVICES=1 python train.py --approach=fotsgatesog --net=fotsgatesog --dataset_name=syn --dataset_root=/home/data/SynthText/ --log_str=continue --use_pretrain --resume_path=/home/cheny/log/fots/Rotation/fotsgatesog/syn/fotsgatesog_pweight0.50_ohem800-200_ltheta10.00_lreg1.00_lrecog1.00_lr0.001_m0.9/model0_9999.pth --detection_batch_size=4 --resize_to=1280 --log_root=~/log/fots/Rotation/  --syn --num_epochs=2 --decay_step 1 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3

# ic15 todo  no need to do
CUDA_VISIBLE_DEVICES=0 python train.py --approach=fotsgatesog --net=fotsgatesog --dataset_name=ic15_13 --dataset_root=~/data/fots/dataset15_13/ --use_pretrain --resume_path= --base_channel=16 --eval_train --eval_test --log_root=~/log/fots/Rotation/ --num_epochs=550 --decay_step 250 400 --ohem --save_inter_cp --save_cp_interval=25 --eval_interval=25 --log_interval_iter=140 --resize_to_val=1800 --num_workers=4 --pos_weight=0.5 --lr=1e-3

