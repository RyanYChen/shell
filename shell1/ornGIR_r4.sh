@ --2.22, gpu4
CUDA_VISIBLE_DEVICES=3 python train.py --approach=syn --net=ornGIR --base_channel=16 --resize_to=1280 --log_root=~/log/fots/Rotation/ornGIR_r4/ --se_r=4 --dataset_root=/home/data/SynthText/ --syn --num_epochs=4 --decay_step 2 --ohem --ohem_num_pi=800 --ohem_reg_num_pi=200 --save_inter_cp_iter --save_cp_interval_iter=5000 --log_interval_iter=500 --debug_save_interval_iter=5000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3



# icdar17
# todo:
CUDA_VISIBLE_DEVICES=3 python train.py --approach=ornGIR_r4 --net=ornGIR --dataset_name=ic17 --dataset_root=~/data/fots/dataset17/ --se_r=4 --base_channel=16 --log_root=~/log/fots/Rotation/ --num_epochs=200 --decay_step 80 160 --ohem --ohem_num_pi=1280 --ohem_reg_num_pi=320 --save_inter_cp --save_cp_interval=10 --log_interval_iter=1000 --num_workers=4 --compute_statistics --pos_weight=0.5 --lr=1e-3
